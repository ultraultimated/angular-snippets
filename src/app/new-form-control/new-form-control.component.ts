import {Component} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-new-form-control',
  templateUrl: './new-form-control.component.html',
  styleUrls: ['./new-form-control.component.css']
})
export class NewFormControlComponent {

  form = new FormGroup({
    topics: new FormArray([])
  });

  addTopic(topic: HTMLInputElement) {
    this.topics.push(new FormControl(topic.value));
    topic.value = '';
  }

  get topics() {
    return this.form.get('topics') as FormArray;
  }

  removeTopic(topic) {
    const index = this.topics.controls.indexOf(topic);
    this.topics.removeAt(index);
  }
}
