import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFormControlComponent } from './new-form-control.component';

describe('NewFormControlComponent', () => {
  let component: NewFormControlComponent;
  let fixture: ComponentFixture<NewFormControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFormControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFormControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
