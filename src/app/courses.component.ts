import { Component } from '@angular/core';
import { CoursesService } from './courses.service';
@Component({
    selector : "courses",
    template: `<h1 style='color:Red;'>
    {{ getTitle() }}
    </h1>
    <ul>
    <li *ngFor="let course of courses">
        {{ course }}
    </li>
    </ul>

    <button class = "btn btn-primary" [class.active] = "isActive">Save</button>
    <button [style.backgroundColor] = "isActive ? 'blue' : 'white'">hello</button>
    <div (click) = "onDivClicked()">
    <button (click) = "onSave($event)">Save ME!</button>
    </div>
    <input #box (keyup.enter) = "onEnter(box.value)"
    (blur) = "onEnter(box.value)">
    <p>{{ value }}</p>
    <input [(ngModel)] = "email" (keyup.enter) = "onKeyUp()" />
    `
})
export class CoursesComponent {
    private title:string = "List of courses";
    courses;
    // private imageUrl:string = "http://www.lorempixel.com/400/200"
    private isActive:boolean = true;
    value = "";
    email = "neelp@gg.com";
    constructor(courseService: CoursesService)
    {
        this.courses = courseService.getCourses();
    }
    getTitle()
    {
        return this.title;
    }
    onSave($event)
    {
        $event.stopPropagation();
        console.log("Save button clicked",$event.isTrusted);
    }
    onDivClicked()
    {
        console.log("Div button clicked");
    }
    onEnter(value:string){
        this.value = value;
    }
    onKeyUp()
    {
        console.log (this.email);
    }
    

}