import {CoursesService} from './courses.service';
import {CoursesComponent} from './courses.component';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CourseComponent} from './course/course.component';
import {ContactFormComponent} from './contact-form/contact-form.component';
import {SignupFormComponent} from './signup-form/signup-form.component';
import {NewFormControlComponent} from './new-form-control/new-form-control.component';
import {SubcategoryComponent} from './subcategory/subcategory.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CourseComponent,
    ContactFormComponent,
    SignupFormComponent,
    NewFormControlComponent,
    SubcategoryComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    CoursesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
