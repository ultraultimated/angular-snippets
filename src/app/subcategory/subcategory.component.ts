import {Component, AfterViewInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.css']
})
export class SubcategoryComponent implements AfterViewInit{
  
  private subcategory: Object;
  private url = 'http://localhost:8080/subcategory/';
  subCategoryObject: any;

  constructor(private http: HttpClient) {
    http.get(this.url.concat('all')).subscribe(response => {
      this.subcategory = response;
    });
  }

  getSubCategory(categoryId){
    let url1 = this.url.concat('category/id/3');
    url1.concat(categoryId);
   
    this.http.get(url1).subscribe(response => {
      this.subCategoryObject = response;
      console.log(this.subCategoryObject);
    });
  }
  ngAfterViewInit(): void {
    this.getSubCategory(3);
  }
 
  


}
